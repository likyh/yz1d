<?php include_once "header.html" ?>
    <div class="content">
        <div class="main_article">
            <h1><?= $results['0']['title']; ?></h1>

            <div class="date_maker_watchtime">
                <span>日期：<?= $results['0']['date']; ?></span>
                <span>编辑人：<?= $results['0']['editor']; ?></span>
                <span>浏览次数：<?= $results['0']['times']; ?></span>
            </div>
            <div class="article">
                <p class="lead">
                   <?= $results['0']['content']; ?>
                </p>
            </div>
            <div class="news_change">
                <span class="before">已经是第一篇</span>
                <span class="before"><a href="">前一篇</a></span>
                <span class="next"><a href="">下一篇</a></span>
            </div>
        </div>
        <div class="outer_news">
            <h2>行业热门文章推荐</h2>
            <ul>
                <?php foreach($results['1'] as $key =>$value){ ?>
                <li class="article_list"><a href="./detail.php?id=<?= $value['id'] ?>"><?= $value['title']; ?></a></li>
                <?php  } ?>
<!--                <li class="article_list"><a href="">测试连接</a></li>-->
<!--                <li class="article_list"><a href="">测试连接</a></li>-->
<!--                <li class="article_list"><a href="">测试连接</a></li>-->
<!--                <li class="article_list"><a href="">测试连接</a></li>-->
<!--                <li class="article_list"><a href="">测试连接</a></li>-->
<!--                <li class="article_list"><a href="">测试连接</a></li>-->
            </ul>
        </div>
    </div>
<?php include_once "footer.html"; ?>