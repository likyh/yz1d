<?php
//    从数据库中读出对应类别的文章，参数为相应类别id
    function getContent($id){
        include_once "./include/dbclass.php";
        $sql='select * from article WHERE class_id = ?';
        $db=new DB();
        $results=$db->getall($sql,$id);
        return $results;
    }
//    从数据库查出类别名称对应的类别id
    function getClass_id($name){
        include_once "./include/dbclass.php";
        $sql='select id from class where name = ?';
        $db=new DB();
        $results=$db->getid($sql,$name);
        return $results;
    }
//  页面分页方法，获取总记录条数，通过循环给页面分页,参数为数据数组，以及要显示的页数
    function Page($results,$page){
        $num=count($results);
        $page_max=(int)$num/15;
        if($page>$page_max||$page==0){
            $page=1;
        }
        $result=array();
        for($i=1;$i<16;$i++){
            $result["$i"]=$results["15*($page-1)+$i"];
        }
        return $result;
    }