<?php
	require_once "../include/dbclass.php";
	$pdo = new DB();
	require_once "tpl/head.html";
	$page_title = "信息安全后台管理系统";
	require_once "tpl/header.html";
	require_once "tpl/register.html";
	require_once "tpl/footer.html";
	
	if (isset($_POST['register']) && isset($_POST['user-name']) && isset($_POST['user-pass']) && isset($_POST['user-pass-repeat'])) {
		$user_name = $_POST['user-name'];
		$user_pass = $_POST['user-pass'];
		$user_pass_repeat = $_POST['user-pass-repeat'];
	
		$q = 'select * from user where name=?';
		$judge = $pdo -> select($q, $user_name);
		if (!$judge && ($user_pass == $user_pass_repeat)) {//账号未被注册并且两次输入的密码一致
			$q = "insert into user (name,password) VALUES (?,?)";
			$result = $pdo -> insert($q, $user_name, $user_pass);
			if ($result) {//成功注册
				header('location:login.php');
				exit ;
			} else {
				header('location:register.php');
				exit ;
			}
		} else {
			header('location:register.php');
			exit ;
		}
	}
