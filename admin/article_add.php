<?php
    require_once"../include/dbclass.php";
    $pdo = new DB();

    require_once "tpl/head.html";
    $page_title = "文章编辑";
    require_once "tpl/header.html";
    $nav_index = "";
    $nav_article = "style='background-color:rgba(255,255,255,.2);'";
    $nav_link = "";
    require_once "tpl/nav.html";
    require_once "tpl/article_add.html";
    require_once "tpl/footer.html";

    if (isset($_POST['submit'])){

            $title = $_POST['title'];
            $editor = $_POST['editor'];
            $date = $_POST['date'];
            $content = $_POST['content'];

        $q = 'insert into article (title,editor,date,content) values (?,?,?,?)';
        $pdo -> insert($q,$title,$editor,$date,$content);
    }