<?php
    require_once"../include/dbclass.php";
    $pdo = new DB();
    require_once "tpl/head.html";
    $page_title = "文章列表";
    require_once "tpl/header.html";
    $nav_index = "";
    $nav_article = "style='background-color:rgba(255,255,255,.2);'";
    $nav_link = "";
    require_once "tpl/nav.html";
    if (isset($_POST['del'])&&isset($_POST['select_id'])){  //删除文章操作
        $del_id = implode(',',$_POST['select_id']);
//        echo $del_id;
        try{
            $q = 'delete from article where id in ('.$del_id.')';
        }catch (PDOException $e){
            echo $e->getMessage();
        }
//        echo $q;
        $pdo -> delete($q);
    }
    $page = $_GET['page'];  // 由form的action 传过来的
    if(!empty($_POST['jump_page'])){     //由分页跳转传过来的
        $page = $_POST['jump_page'];
    }
    $page_offset = 2;   //偏移量
    $page_size = 10;    //每页显示最大行数
    $q = 'select * from article';
    $rows = count($pdo->getall($q)); //总记录条数
    $total_page = ceil($rows/$page_size);  //总页数
    $show_page = 5;
    $q = 'select * from article limit '.($page-1)*$page_size .','.$page_size;  //limit 起始位置，显示条数
//    echo $q;
    $results = $pdo->getall($q);    //当前页要显示数据存在results中


    if(empty($results)){    //当前页删没了就跳到前一页
        header('location:article.php?page='.($page-1).'');
        exit;
    }

/*  以下分页逻辑  */

//由于用到了bootstrap btn样式,而bootstrap对btn设有左浮动会影响到下面分页样式，故应清除此浮动 ！！！
//由于用到了bootstrap btn样式,而bootstrap对btn设有左浮动会影响到下面分页样式，故应清除此浮动 ！！！
//由于用到了bootstrap btn样式,而bootstrap对btn设有左浮动会影响到下面分页样式，故应清除此浮动 ！！！


    if($page > 1){
        $page_banner = '<button type="button" class="btn btn-default" name="page-turning" onclick=window.location.href="'.$_SERVER['PHP_SELF'].'?page=1">首页</button>';
        $page_banner .= '<button type="button" class="btn btn-default" style="margin-right: 5px;" name="page-turning" onclick=window.location.href="'.$_SERVER['PHP_SELF'].'?page='.($page-1). '">上一页</button>';
    }else{   //当前页为第一页时屏蔽上一页和首页链接
        $page_banner = '<button type="button" class="btn btn-default" name="page-turning" onclick=window.location.href="#">首页</button>';
        $page_banner .= '<button type="button" class="btn btn-default" style="margin-right: 5px;" name="page-turning" onclick=window.location.href="#">上一页</button>';

    }

    /*  中间显示页逻辑  */
    $start = 1;
    $end = $total_page;
    if($total_page > $show_page){
        if($page > $page_offset+1){
            $page_banner .= '<span style="margin-left: 5px;margin-right: 5px;">...</span>';
        }
        if($page > $page_offset){
            $start = $page-$page_offset;
            $end = $total_page>$page+$page_offset ?
                    $page+$page_offset :
                    $total_page;
        }else{
            $start = 1;
            $end = $total_page>$show_page ?
                    $show_page :
                    $total_page;
        }
        if($page+$page_offset > $total_page){
            $start = $start-($page+$page_offset-$total_page);
        }
    }

    for ($i=$start;$i<=$end;$i++){
        if($i == $page){
            $page_banner .= '<button type="button" class="btn btn-default btn-success" style="margin-left: 5px;margin-right:5px;" name="page-turning" onclick=window.location.href="'.$_SERVER['PHP_SELF'].'?page='.$i.'">'.$i.'</button>';
        }else{
                $page_banner .= '<button type="button" class="btn btn-default" style="margin-left: 5px;margin-right: 5px;" name="page-turning" onclick=window.location.href="'.$_SERVER['PHP_SELF'].'?page='.$i.'">'.$i.'</button>';
        }



    }
    if($total_page > $page+$page_offset){
        $page_banner .= '<span style="margin-left: 5px;margin-right: 10px;">...</span>';
    }
    if($page < $total_page){
        $page_banner .= '<button type="button" class="btn btn-default" name="page-turning" style="margin-left: 5px;" onclick=window.location.href="'.$_SERVER['PHP_SELF'].'?page='.($page+1). '">下一页</button>';
        $page_banner .= '<button type="button" class="btn btn-default" name="page-turning" onclick=window.location.href="'.$_SERVER['PHP_SELF'].'?page='.$total_page.'">末页</button>';

    }else{   //当前页为最后一页时屏蔽下一页和末页链接
        $page_banner .= '<button type="button" class="btn btn-default" name="page-turning" style="margin-left: 5px;" onclick=window.location.href="#">下一页</button>';
        $page_banner .= '<button type="button" class="btn btn-default" name="page-turning" onclick=window.location.href="#">末页</button>';
    }
    $page_banner .= '<span style="margin-left: 10px;">共有 '.$total_page.' 页 / '.$rows.' 篇文章</span>';


    $page_banner .= '<span style="margin-left: 10px;font-size: 15px;">到第<input type="text" size="2" name="jump_page"  />页</span>';

    $page_banner .= '<input type="submit" style="margin-left: 10px;" value="确定" />';
    require_once "tpl/article.html";
    require_once "tpl/footer.html";
	



