<?php
	class DB {
		protected static $dbh;
    //数据库连接，并建立pdo对象，无参数
		public function getpdo (){
            if(isset($dbh)){
                return self::$dbh;
            }else{
			$dsn='mysql:dbname=myapp;host=127.0.0.1;port=3306';
			$user='root';
			$password='';
			try{
				$dbh=new PDO($dsn,$user,$password);
				$dbh->query('set names utf8');
                return $dbh;
			}catch(PDOException $e){
				echo "数据库连接失败".$e->getMessage();
			}
            return null;
		}}
        //取出相关id操作
        public function getid (){
            $row=func_num_args();
            $result =func_get_args();
            $pdo=$this->getpdo();
            $stmt=$pdo->prepare($result['0']);
            for($i = 1 ; $i < $row ; $i++){
                $stmt->bindValue($i ,$result["$i"] );
            }
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_NUM);
            $result=$stmt->fetch();
            if($result){
                return $result['0'];
            }else{
                return null;
            }
        }
        //查询操作，返回是否查询到，查询到但回查到的id；
		public function select () {
            $row=func_num_args();
            $result =func_get_args();
            $pdo=$this->getpdo();
            $stmt=$pdo->prepare($result['0']);
//            $stmt->bindValue(1,$id);
            for($i = 1 ; $i < $row ; $i++){
                $stmt->bindValue($i ,$result["$i"] );
            }
            $stmt->execute();
            if($stmt->rowCount()){
                return $stmt->rowCount();
            }else return 0;
		}
        //得到一条数据操作，参数为sql语句和相应id值；返回值为数组；
		public function getone() {
            $row=func_num_args();
            $result =func_get_args();
			$pdo=$this->getpdo();
			$re=$pdo->prepare($result['0']);
//            $re->bindValue(1,$id);
            for($i = 1 ; $i < $row ; $i++){
                $re->bindValue($i ,$result["$i"] );
            }
            $re->execute();
            $re->setFetchMode(PDO::FETCH_ASSOC);
            $ress=$re->fetch();
			if(!$ress){
				echo "读取数据失败";
			}
			return $ress;
		}
        //读取所有数据，返回值为数组；
		public function getall(){
            $row=func_num_args();
            $result =func_get_args();
			$pdo=$this->getpdo();
			$res=$pdo->prepare($result['0']);
            for($i = 1 ; $i < $row ; $i++){
                $res->bindValue($i ,$result["$i"] );
            }
            $res->execute();
            $res->setFetchMode(PDO::FETCH_ASSOC);
            $ress=$res->fetchAll();
			if(!$ress){
				echo "读取数据失败";
			}
			return $ress;
		}
        //插入操作；返回值为true或者false；
		public function insert(){
            $row=func_num_args();
            $result =func_get_args();
			$pdo=$this->getpdo();
			$stmt=$pdo->prepare($result['0']);
//            $stmt->bindValue(1,$id);
//            $stmt->bindValue(2,$value);
            for($i = 1 ; $i < $row ; $i++){
                $stmt->bindValue($i ,$result["$i"] );
            }
            $stmt->execute();
            $insert=$stmt->rowCount();
			if(!$insert){
				echo "插入失败";
			}
			return $insert;
		}
        //修改数据操作，返回受影响的列数；
		public function update (){
            $row=func_num_args();
            $result =func_get_args();
			$pdo=$this->getpdo();
            $stmt=$pdo->prepare($result['0']);
//            $stmt->bindValue(1,$value);
//            $stmt->bindValue(2,$id1);
            for($i = 1 ; $i < $row ; $i++){
                $stmt->bindValue($i ,$result["$i"] );
            }
            $stmt->execute();
            $update=$stmt->rowCount();
			if(!$update){
				echo "数据更新失败";
			}
			return $update;
		}
        //数据删除操作,返回为收影响的条数；
		public function delete (){
            $row=func_num_args();
            $result =func_get_args();
			$pdo=$this->getpdo();
            $stmt=$pdo->prepare($result['0']);
//            $stmt->bindValue(1,$id);
            for($i = 1 ; $i < $row ; $i++){
                $stmt->bindValue($i ,$result["$i"] );
            }
            $stmt->execute();
            $delete=$stmt->rowCount();
			if(!$delete){
				echo "数据删除失败";
			}
			return $delete;
		}
        //析构函数
        public function __desruct(){
            $dbh=null;
        }

}
//$db=new DB;
//查询操作完成
//$valve='166';
//$value1='你你您';
//$sql='select * from content where id=? and content =? ';
//$r=$db->select($sql,$valve,$value1);
//print_r($r);
////get 操作完成；；
//$sql='select * from content';
//$value='166';
//$value1='你你您';
//$ress=$db->getall($sql);
//print_r($ress);
//foreach($r as $row){echo $row['content']."<br>";}
//$sql1='select * fron content where id = 166';
//$r=$db->getone($sql1);
//print_r($r);
//插入操作完成。
//$sql='insert into content (id,content) VALUE (?,?)';
//$id='20';
//$value='你好吗';
//$insert=$db->insert($sql,$id,$value);
//更新操作
//$sql='update content set content = ? WHERE  id = ?';
//$id='169';
//$value='lalallallal';
//$update=$db->update($sql,$value,$id);
//print_r($update);
//数据删除操作；
//$sql='delete from content where id = ? and content=?';
//$id='166';
//$value='你你您';
//$delete=$db->delete($sql,$id,$value);
//print_r($delete);

