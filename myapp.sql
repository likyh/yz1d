-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016-05-15 08:59:04
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `myapp`
--

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '文章标题',
  `date` date NOT NULL COMMENT '文章发布',
  `editor` varchar(255) NOT NULL COMMENT '文章编辑者',
  `times` int(11) NOT NULL COMMENT '文章浏览次数',
  `content` text NOT NULL COMMENT '文章内容',
  `images_path` varchar(256) DEFAULT NULL COMMENT '内容图片路径',
  `creat_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立文章时间',
  `site_id` int(11) NOT NULL COMMENT '表明文章来自的站点',
  `class_id` int(11) NOT NULL COMMENT '文章分类',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `article`
--

INSERT INTO `article` (`id`, `title`, `date`, `editor`, `times`, `content`, `images_path`, `creat_time`, `site_id`, `class_id`, `is_delete`) VALUES
(1, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '2016-04-19', 'BitSec', 249, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(2, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '2016-04-19', 'BitSec', 249, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(3, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '2016-04-20', 'BitSec', 249, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(4, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 249, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(5, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 249, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(6, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 249, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(7, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 249, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(8, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 248, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(9, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 5, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(10, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 222, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(11, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 222, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(12, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 249, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(13, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '2016-04-20', 'BitSec', 249, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(14, '【紧急通知】“古檀杯”大学生网络安全技能大赛初赛时间更改通知', '0000-00-00', 'BitSec', 222, '因学生近期准备考试及部分学生参加相关会议的原因，“古檀杯”大学生网络安全技能大赛的初赛时间更改到5月14-15日进行，同时竞赛报名截止时间顺延至5月10日24时。欢迎来自全国高校的正式学籍的学生报名参加。\r\n        给大家带来不便，敬请谅解。\r\n        进一步的竞赛通知，请关注竞赛官网http://is.cumt.edu.cn。\r\n\r\n                                                                                            竞赛组委会\r\n                                                                                            2016.4.19', NULL, '2016-05-08 07:16:12', 0, 0, 0),
(15, '测试文章', '2016-05-13', 'ccccc', 333, '模拟播报了同城voooouvou哦哦', NULL, '2016-05-10 14:31:10', 0, 0, 0),
(16, '小小', '2016-05-17', 'vvvvvvv', 15151, 'vvwavawvwvvDev', NULL, '2016-05-10 14:31:37', 0, 0, 0),
(17, '快开始', '2016-05-19', 'CUMT', 2525, '那你男男女女男男女女男男女女男男女女男男女女男男女女男男女女男男女女男男女女男男女女', NULL, '2016-05-08 14:06:18', 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `name` varchar(256) NOT NULL COMMENT '类别名称',
  `site_id` int(11) NOT NULL COMMENT '类别来源',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '类别id',
  `name` varchar(256) NOT NULL COMMENT '对应网址名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL COMMENT '用户账号',
  `password` varchar(256) NOT NULL COMMENT '用户密码',
  `login_ip` varchar(256) NOT NULL COMMENT '用户本次登录ip',
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用户本次登录时间',
  `is_lock` int(11) NOT NULL DEFAULT '0' COMMENT '用户是否锁定',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
